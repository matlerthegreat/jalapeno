use std::ops::Deref;

use super::{
    common::{Gen, Offset},
    weak_proxy::WeakProxy,
};

#[derive(Debug, Clone, Copy)]
pub struct StrongProxy<'a, T> {
    pub(super) gen: Gen,
    pub(super) offset: Offset,
    pub(super) value: &'a T,
}

impl<'a, T> StrongProxy<'a, T> {
    pub fn downgrade(this: &StrongProxy<'a, T>) -> WeakProxy {
        WeakProxy {
            gen: this.gen,
            offset: this.offset,
        }
    }
}

impl<'a, T> Deref for StrongProxy<'a, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.value
    }
}
