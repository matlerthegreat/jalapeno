use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign};

use self::{
    matrix::Matrix,
    topology::{Digital, Euclidean, Projective},
    vector::Vector,
};

pub mod matrix;
pub mod point;
pub mod quaterion;
pub mod topology;
pub mod vector;

pub type Integer = i32;
pub type Scalar = f32;

pub type Vector2 = Vector<Euclidean, 2>;
pub type Vector3 = Vector<Euclidean, 3>;
pub type Vector4 = Vector<Projective, 4>;

pub type IVector2 = Vector<Digital, 2>;
pub type IVector3 = Vector<Digital, 3>;
pub type IVector4 = Vector<Digital, 4>;

pub type Matrix3 = Matrix<Scalar, 3, 3>;
pub type Matrix4 = Matrix<Scalar, 4, 4>;

// Skip the Neg trait to allow for unsigned integers
pub trait Field<T>:
    Add<Output = Self>
    + Sub<Output = Self>
    + Mul<Output = Self>
    + Div<Output = Self>
    + AddAssign
    + SubAssign
    + MulAssign
    + DivAssign
    + Copy
    + Default
{
    const ZERO: Self;
    const ONE: Self;
}

impl Field<Scalar> for Scalar {
    const ZERO: Self = 0.0;
    const ONE: Self = 1.0;
}

impl Field<Integer> for Integer {
    const ZERO: Self = 0;
    const ONE: Self = 1;
}

impl Field<usize> for usize {
    const ZERO: Self = 0;
    const ONE: Self = 1;
}

impl Field<u8> for u8 {
    const ZERO: Self = 0;
    const ONE: Self = 1;
}
