use core::panic;
use std::iter::{Enumerate, FilterMap};

use common::{Error, Gen, Offset};
pub use mut_proxy::MutProxy;
pub use strong_proxy::StrongProxy;
pub use weak_proxy::WeakProxy;

pub mod common;
pub mod mut_proxy;
pub mod strong_proxy;
pub mod weak_proxy;

#[derive(Debug)]
pub enum Entry<T> {
    Free { gen: Gen, next: Option<Offset> },
    Taken { gen: Gen, value: T },
}

#[derive(Debug)]
pub struct Arena<T> {
    entries: Vec<Entry<T>>,
    free: Option<Offset>,
}

impl<T> Arena<T> {
    pub fn insert(&mut self, value: T) -> WeakProxy {
        if let Some(offset) = self.free {
            match self.entries.get(offset as usize).unwrap() {
                &Entry::Free { gen, next } => {
                    self.free = next;
                    self.entries[offset as usize] = Entry::Taken { gen, value };
                    WeakProxy { gen, offset }
                }
                _ => panic!(),
            }
        } else {
            self.entries.push(Entry::Taken { gen: 0, value });
            WeakProxy {
                gen: 0,
                offset: (self.entries.len() - 1).try_into().unwrap(),
            }
        }
    }

    pub fn remove(&mut self, proxy: &WeakProxy) -> Result<(), Error> {
        match self.entries[proxy.offset as usize] {
            Entry::Taken { gen, .. } if gen == proxy.gen => {
                self.entries[proxy.offset as usize] = Entry::Free {
                    gen: gen + 1,
                    next: self.free,
                };
                self.free = Some(proxy.offset);
                Ok(())
            }
            Entry::Taken { .. } => Err(Error::GenMissmatch),
            Entry::Free { .. } => Ok(()),
        }
    }

    pub fn upgrade<'a>(&'a self, proxy: &WeakProxy) -> Result<StrongProxy<'a, T>, Error> {
        let value = self.get(proxy)?;
        Ok::<StrongProxy<'a, T>, Error>(StrongProxy {
            gen: proxy.gen,
            offset: proxy.offset,
            value,
        })
    }

    pub fn upgrade_mut<'a>(&'a mut self, proxy: &WeakProxy) -> Result<MutProxy<'a, T>, Error> {
        let value = self.get_mut(proxy)?;
        Ok::<MutProxy<'a, T>, Error>(MutProxy {
            gen: proxy.gen,
            offset: proxy.offset,
            value,
        })
    }

    pub fn upgrade_many_mut<'a, const N: usize>(
        &'a mut self,
        proxies: &[WeakProxy; N],
    ) -> [MutProxy<'a, T>; N] {
        if !self.get_many_check_valid(proxies) {
            panic!()
        }

        let ptr = self.entries.as_mut_ptr();
        proxies.map(|proxy| {
            let entry = unsafe { ptr.add(proxy.offset as usize).as_mut().unwrap() };
            match entry {
                Entry::Taken { gen, ref mut value } if *gen == proxy.gen => MutProxy {
                    gen: proxy.gen,
                    offset: proxy.offset,
                    value,
                },
                Entry::Taken { .. } => panic!(),
                Entry::Free { .. } => panic!(),
            }
        })
    }

    pub fn values(&self) -> impl Iterator<Item = &T> + Clone {
        self.entries.iter().filter_map(|entry| match entry {
            Entry::Taken { value, .. } => Some(value),
            _ => None,
        })
    }

    pub fn values_mut(&mut self) -> impl Iterator<Item = &mut T> {
        self.entries.iter_mut().filter_map(|entry| match entry {
            Entry::Taken { value, .. } => Some(value),
            _ => None,
        })
    }

    pub fn iter(&self) -> impl Iterator<Item = StrongProxy<T>> + Clone {
        self.entries
            .iter()
            .enumerate()
            .filter_map(|(offset, entry)| match entry {
                Entry::Taken { value, gen } => Some(StrongProxy {
                    gen: *gen,
                    offset: offset as Offset,
                    value,
                }),
                _ => None,
            })
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = MutProxy<T>> {
        self.entries
            .iter_mut()
            .enumerate()
            .filter_map(|(offset, entry)| match entry {
                Entry::Taken { value, gen } => Some(MutProxy {
                    gen: *gen,
                    offset: offset as Offset,
                    value,
                }),
                _ => None,
            })
    }

    pub fn extend<'a>(
        &'a mut self,
        values: impl IntoIterator<Item = T> + 'a,
    ) -> impl Iterator<Item = WeakProxy> + 'a {
        values.into_iter().map(|value| self.insert(value))
    }

    pub fn get(&self, proxy: &WeakProxy) -> Result<&T, Error> {
        match &self.entries[proxy.offset as usize] {
            Entry::Taken { gen, value } if *gen == proxy.gen => Ok(value),
            Entry::Taken { .. } => Err(Error::GenMissmatch),
            Entry::Free { .. } => Err(Error::Free),
        }
    }

    pub fn get_mut(&mut self, proxy: &WeakProxy) -> Result<&mut T, Error> {
        match self.entries[proxy.offset as usize] {
            Entry::Taken { gen, ref mut value } if gen == proxy.gen => Ok(value),
            Entry::Taken { .. } => Err(Error::GenMissmatch),
            Entry::Free { .. } => Err(Error::Free),
        }
    }

    pub fn get_many_mut<const N: usize>(&mut self, proxies: &[WeakProxy; N]) -> [&mut T; N] {
        if !self.get_many_check_valid(proxies) {
            panic!()
        }

        let ptr = self.entries.as_mut_ptr();
        proxies.map(|proxy| {
            let entry = unsafe { ptr.add(proxy.offset as usize).as_mut().unwrap() };
            match entry {
                Entry::Taken { gen, ref mut value } if *gen == proxy.gen => value,
                Entry::Taken { .. } => panic!(),
                Entry::Free { .. } => panic!(),
            }
        })
    }

    fn get_many_check_valid<const N: usize>(&self, proxies: &[WeakProxy; N]) -> bool {
        let mut valid = true;
        for (i, &proxy) in proxies.iter().enumerate() {
            valid &= (proxy.offset as usize) < self.entries.len();
            for &proxy2 in &proxies[..i] {
                valid &= proxy != proxy2;
            }
        }
        valid
    }
}

impl<T> Default for Arena<T> {
    fn default() -> Self {
        Arena {
            entries: Vec::new(),
            free: None,
        }
    }
}

impl<T> From<T> for Entry<T> {
    fn from(value: T) -> Self {
        Entry::Taken { gen: 0, value }
    }
}

impl<T> FromIterator<T> for Arena<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let entries: Vec<Entry<T>> = iter.into_iter().map(Entry::from).collect();
        Arena {
            entries,
            free: None,
        }
    }
}

impl<T> IntoIterator for Arena<T> {
    type Item = (WeakProxy, T);

    type IntoIter = FilterMap<
        Enumerate<std::vec::IntoIter<Entry<T>>>,
        fn((usize, Entry<T>)) -> Option<(WeakProxy, T)>,
    >;

    fn into_iter(self) -> Self::IntoIter {
        self.entries
            .into_iter()
            .enumerate()
            .filter_map(|(index, entry)| match entry {
                Entry::Free { .. } => None,
                Entry::Taken { value, gen } => Some((
                    WeakProxy {
                        gen,
                        offset: index as Offset,
                    },
                    value,
                )),
            })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_insert() {
        let mut arena: Arena<i32> = Arena::default();

        let index = arena.insert(5);

        assert_eq!(arena.get(&index), Ok(&5));
    }

    #[test]
    fn test_edit() {
        let mut arena: Arena<i32> = Arena::default();

        let index = arena.insert(5);
        if let Ok(element) = arena.get_mut(&index) {
            *element = 7;
        }

        assert_eq!(arena.get(&index), Ok(&7));
    }
}
