use std::{
    array,
    borrow::{Borrow, BorrowMut},
    ops::{
        Add, AddAssign, Deref, DerefMut, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub,
        SubAssign,
    },
};

use super::{
    point::Point,
    topology::{
        Digital, Euclidean, InnerProductSpace, MetricSpace, Projective, Space, VectorSpace,
        XyzVector,
    },
    Field, Scalar,
};

// Coordinate vector
#[repr(transparent)]
#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Vector<S: Space<D>, const D: usize>(pub [S::Coordinate; D]);

pub trait LinearTransform<const M: usize, const N: usize> {
    type From: Space<M>;
    type To: Space<N>;

    fn map(self, v: Vector<Self::From, M>) -> Vector<Self::To, N>;
}

impl<const M: usize, const N: usize, T: LinearTransform<M, N>> From<(T, Vector<T::From, M>)>
    for Vector<T::To, N>
{
    fn from((t, v): (T, Vector<T::From, M>)) -> Self {
        t.map(v)
    }
}

impl From<Vector<Projective, 4>> for Vector<Euclidean, 3> {
    fn from(value: Vector<Projective, 4>) -> Self {
        Self([value.x, value.y, value.z]) / value.w
    }
}

impl From<Vector<Euclidean, 3>> for Vector<Projective, 4> {
    fn from(value: Vector<Euclidean, 3>) -> Self {
        Self([value.x, value.y, value.z, Scalar::ONE])
    }
}

impl<const D: usize, S: MetricSpace<D>> Vector<S, D>
where
    S::Coordinate: Copy,
{
    pub fn length_squared(self) -> S::F {
        S::length_squared(self)
    }

    pub fn length(self) -> S::F {
        S::length(self)
    }
}

impl<const D: usize, S: InnerProductSpace<D>> Vector<S, D>
where
    S::Coordinate: Field<S::Coordinate>,
{
    pub fn dot(self, other: Self) -> S::Coordinate {
        S::dot(self, other)
    }
}

impl Vector<Euclidean, 3> {
    pub fn normalized(self) -> Self {
        self / self.length()
    }

    pub fn normalize(&mut self) -> Scalar {
        let len = self.length();
        *self = self.normalized();
        len
    }

    pub fn cross(self, other: Self) -> Self {
        let x = self.y * other.z - self.z * other.y;
        let y = self.z * other.x - self.x * other.z;
        let z = self.x * other.y - self.y * other.x;

        Self([x, y, z])
    }
}

impl Vector<Digital, 3> {
    pub fn cross(self, other: Self) -> Self {
        let x = self.y * other.z - self.z * other.y;
        let y = self.z * other.x - self.x * other.z;
        let z = self.x * other.y - self.y * other.x;

        Self([x, y, z])
    }
}

impl<T: Field<T>, S: Space<2, Coordinate = T, Coordinates = XyzVector<T>>> Vector<S, 2> {
    pub const OX: Self = Vector([T::ONE, T::ZERO]);
    pub const OY: Self = Vector([T::ZERO, T::ONE]);
}

impl<T: Field<T>, S: Space<3, Coordinate = T, Coordinates = XyzVector<T>>> Vector<S, 3> {
    pub const OX: Self = Vector([T::ONE, T::ZERO, T::ZERO]);
    pub const OY: Self = Vector([T::ZERO, T::ONE, T::ZERO]);
    pub const OZ: Self = Vector([T::ZERO, T::ZERO, T::ONE]);
}

impl<T: Field<T>, S: Space<4, Coordinate = T, Coordinates = XyzVector<T>>> Vector<S, 4> {
    pub const OX: Self = Vector([T::ONE, T::ZERO, T::ZERO, T::ZERO]);
    pub const OY: Self = Vector([T::ZERO, T::ONE, T::ZERO, T::ZERO]);
    pub const OZ: Self = Vector([T::ZERO, T::ZERO, T::ONE, T::ZERO]);
    pub const OW: Self = Vector([T::ZERO, T::ZERO, T::ZERO, T::ONE]);
}

impl<const D: usize, S: Space<D>> Default for Vector<S, D>
where
    S::Coordinate: Default,
{
    fn default() -> Self {
        Self(array::from_fn(|_| Default::default()))
    }
}

impl<const D: usize, S: Space<D>> Clone for Vector<S, D>
where
    S::Coordinate: Clone,
{
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<const D: usize, S: Space<D>> Copy for Vector<S, D> where S::Coordinate: Copy {}

impl<const D: usize, S: Space<D>> Deref for Vector<S, D> {
    type Target = S::Coordinates;

    fn deref(&self) -> &Self::Target {
        S::as_coordinates(&self.0)
    }
}

impl<const D: usize, S: Space<D>> DerefMut for Vector<S, D> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        S::as_mut_coordinates(&mut self.0)
    }
}

impl<const D: usize, S: Space<D>> Borrow<[S::Coordinate; D]> for Vector<S, D> {
    fn borrow(&self) -> &[S::Coordinate; D] {
        &self.0
    }
}

impl<const D: usize, S: Space<D>> BorrowMut<[S::Coordinate; D]> for Vector<S, D> {
    fn borrow_mut(&mut self) -> &mut [S::Coordinate; D] {
        &mut self.0
    }
}

impl<const D: usize, S: Space<D>> Borrow<[S::Coordinate; D]> for &Vector<S, D> {
    fn borrow(&self) -> &[S::Coordinate; D] {
        &self.0
    }
}

impl<const D: usize, S: Space<D>> Borrow<[S::Coordinate; D]> for &mut Vector<S, D> {
    fn borrow(&self) -> &[S::Coordinate; D] {
        &self.0
    }
}

impl<const D: usize, S: Space<D>> BorrowMut<[S::Coordinate; D]> for &mut Vector<S, D> {
    fn borrow_mut(&mut self) -> &mut [S::Coordinate; D] {
        &mut self.0
    }
}

impl<const D: usize, S: Space<D>> From<[S::Coordinate; D]> for Vector<S, D> {
    fn from(value: [S::Coordinate; D]) -> Self {
        Self(value)
    }
}

impl<const D: usize, S: Space<D>> Index<usize> for Vector<S, D> {
    type Output = S::Coordinate;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<const D: usize, S: Space<D>> IndexMut<usize> for Vector<S, D> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

impl<const D: usize, S: Space<D>> IntoIterator for Vector<S, D> {
    type Item = S::Coordinate;

    type IntoIter = array::IntoIter<Self::Item, D>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<const D: usize, S: Space<D>> From<Point<S, D>> for Vector<S, D> {
    fn from(value: Point<S, D>) -> Self {
        Vector(value.0)
    }
}

impl<const D: usize, S: VectorSpace<D>> Add for Vector<S, D> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Vector(S::add(self, rhs))
    }
}

impl<const D: usize, S: VectorSpace<D>> AddAssign for Vector<S, D> {
    fn add_assign(&mut self, rhs: Self) {
        S::add_assign(self, rhs)
    }
}

impl<const D: usize, S: VectorSpace<D>> Sub for Vector<S, D> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Vector(S::sub(self, rhs))
    }
}

impl<const D: usize, S: VectorSpace<D>> SubAssign for Vector<S, D> {
    fn sub_assign(&mut self, rhs: Self) {
        S::sub_assign(self, &rhs.0)
    }
}

impl<const D: usize, S: VectorSpace<D>> Mul<Vector<S, D>> for S::F {
    type Output = Vector<S, D>;

    fn mul(self, rhs: Vector<S, D>) -> Self::Output {
        Vector(S::mul(&rhs.0, self))
    }
}

impl<const D: usize, S: VectorSpace<D>> MulAssign<S::F> for Vector<S, D> {
    fn mul_assign(&mut self, rhs: S::F) {
        S::mul_assign(self, rhs)
    }
}

impl<const D: usize, S: VectorSpace<D>> Div<S::F> for Vector<S, D> {
    type Output = Self;

    fn div(self, rhs: S::F) -> Self::Output {
        Vector(S::div(self, rhs))
    }
}

impl<const D: usize, S: VectorSpace<D>> DivAssign<S::F> for Vector<S, D> {
    fn div_assign(&mut self, rhs: S::F) {
        S::div_assign(self, rhs)
    }
}

// Field is intentionally missing the Neg trait, so add this one extra
impl<const D: usize, S: VectorSpace<D>> Neg for Vector<S, D>
where
    <S as Space<D>>::Coordinate: Copy + Neg<Output = <S as Space<D>>::Coordinate>,
{
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self(array::from_fn(|i| -self[i]))
    }
}

#[cfg(test)]
mod tests {
    use crate::math::*;

    use super::*;

    #[test]
    fn addition() {
        let v: Vector<Euclidean, 3> = Vector([1.0, 2.0, 3.0]);
        let u = Vector([6.0, 5.0, 4.0]);

        let expected: Vector<Euclidean, 3> = Vector([7.0, 7.0, 7.0]);

        assert_eq!(v + u, expected);
    }

    #[test]
    fn normalization() {
        let mut v: Vector3 = Vector([1., 2., 3.]);
        let expected_n = v.normalized();
        let expected_len = v.length();

        let len = v.normalize();

        assert_eq!(v, expected_n);
        assert_eq!(len, expected_len);
    }

    #[test]
    fn cross() {
        let x = Vector3::OX;
        let y = Vector3::OY;
        let z = Vector3::OZ;

        assert_eq!(x.cross(y), z);
    }
}
