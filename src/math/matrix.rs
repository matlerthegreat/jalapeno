use std::{
    array,
    ops::{Index, IndexMut, Mul},
};

use super::{
    quaterion::Quaterion,
    topology::{Euclidean, Projective, Space},
    vector::Vector,
    Field, Scalar,
};

// TODO?
/*pub trait Transform<const M: usize, const N: usize> {
    type Coordinate;
    type From: Space<N, Coordinate = Self::Coordinate>;
    type To: Space<M, Coordinate = Self::Coordinate>;
}*/

pub trait LinearTransform<const M: usize, const N: usize> {
    type Coordinate;
    type From: Space<N, Coordinate = Self::Coordinate>;
    type To: Space<M, Coordinate = Self::Coordinate>;
}

#[repr(transparent)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Matrix<T, const M: usize, const N: usize>(pub [[T; N]; M]);

impl<T: Copy, const M: usize, const N: usize> Matrix<T, M, N> {
    // TODO: T: Copy is technically not needed here, possibly with some unsafe code
    pub fn transpose(self) -> Matrix<T, N, M> {
        Matrix(array::from_fn(|n| array::from_fn(|m| self[n][m])))
    }
}

impl Matrix<Scalar, 3, 3> {
    pub const fn identity() -> Self {
        Self([
            [Scalar::ONE, Scalar::ZERO, Scalar::ZERO],
            [Scalar::ZERO, Scalar::ONE, Scalar::ZERO],
            [Scalar::ZERO, Scalar::ZERO, Scalar::ONE],
        ])
    }

    pub fn scaling(scale: Vector<Euclidean, 3>) -> Self {
        Self([
            [scale[0], Scalar::ZERO, Scalar::ZERO],
            [Scalar::ZERO, scale[1], Scalar::ZERO],
            [Scalar::ZERO, Scalar::ZERO, scale[2]],
        ])
    }

    pub fn from_rotation(q: Quaterion) -> Self {
        Self([
            [
                1.0 - 2.0 * (q.j * q.j + q.k * q.k),
                2.0 * (q.i * q.j + q.k * q.r),
                2.0 * (q.i * q.k - q.j * q.r),
            ],
            [
                2.0 * (q.i * q.j - q.k * q.r),
                1.0 - 2.0 * (q.i * q.i + q.k * q.k),
                2.0 * (q.j * q.k + q.i * q.r),
            ],
            [
                2.0 * (q.i * q.k + q.j * q.r),
                2.0 * (q.j * q.k - q.i * q.r),
                1.0 - 2.0 * (q.i * q.i + q.j * q.j),
            ],
        ])
    }
}

impl Matrix<Scalar, 4, 4> {
    pub const fn identity() -> Self {
        Self([
            [Scalar::ONE, Scalar::ZERO, Scalar::ZERO, Scalar::ZERO],
            [Scalar::ZERO, Scalar::ONE, Scalar::ZERO, Scalar::ZERO],
            [Scalar::ZERO, Scalar::ZERO, Scalar::ONE, Scalar::ZERO],
            [Scalar::ZERO, Scalar::ZERO, Scalar::ZERO, Scalar::ONE],
        ])
    }

    pub fn from_translation(translation: Vector<Euclidean, 3>) -> Self {
        Self([
            [Scalar::ONE, Scalar::ZERO, Scalar::ZERO, Scalar::ZERO],
            [Scalar::ZERO, Scalar::ONE, Scalar::ZERO, Scalar::ZERO],
            [Scalar::ZERO, Scalar::ZERO, Scalar::ONE, Scalar::ZERO],
            [translation[0], translation[1], translation[2], Scalar::ONE],
        ])
    }

    // Projects points within perspective view to box with x and y in range [-1, 1] and z in [0, 1]
    pub fn from_perspective(fov: Scalar, aspect_ratio: Scalar, near: Scalar, far: Scalar) -> Self {
        let f = 1.0 / (fov / 2.0).tan();
        let range = near - far;

        Self([
            [f / aspect_ratio, 0.0, 0.0, 0.0],
            [0.0, f, 0.0, 0.0],
            [0.0, 0.0, (near + far) / range, -1.0],
            [0.0, 0.0, (near * far) / range, 0.0],
        ])
    }
}

impl<T, const M: usize, const N: usize> Index<usize> for Matrix<T, M, N> {
    type Output = [T; N];

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<T, const M: usize, const N: usize> IndexMut<usize> for Matrix<T, M, N> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

impl From<Matrix<Scalar, 3, 3>> for Matrix<Scalar, 4, 4> {
    fn from(val: Matrix<Scalar, 3, 3>) -> Self {
        Self([
            [val[0][0], val[0][1], val[0][2], Scalar::ZERO],
            [val[1][0], val[1][1], val[1][2], Scalar::ZERO],
            [val[2][0], val[2][1], val[2][2], Scalar::ZERO],
            [Scalar::ZERO, Scalar::ZERO, Scalar::ZERO, Scalar::ONE],
        ])
    }
}

impl Mul<Vector<Euclidean, 3>> for Matrix<Scalar, 3, 3> {
    type Output = Vector<Euclidean, 3>;

    fn mul(self, rhs: Vector<Euclidean, 3>) -> Self::Output {
        rhs[0] * Vector(self[0]) + rhs[1] * Vector(self[1]) + rhs[2] * Vector(self[2])
    }
}

impl Mul<Vector<Projective, 4>> for Matrix<Scalar, 4, 4> {
    type Output = Vector<Projective, 4>;

    fn mul(self, rhs: Vector<Projective, 4>) -> Self::Output {
        rhs[0] * Vector(self[0])
            + rhs[1] * Vector(self[1])
            + rhs[2] * Vector(self[2])
            + rhs[3] * Vector(self[3])
    }
}

impl Mul<Matrix<Scalar, 4, 4>> for Matrix<Scalar, 4, 4> {
    type Output = Matrix<Scalar, 4, 4>;

    fn mul(self, rhs: Matrix<Scalar, 4, 4>) -> Self::Output {
        Matrix(array::from_fn(|l| {
            (self * Vector::<Projective, 4>(rhs[l])).0
        }))
    }
}

/*impl<T: Field<T>> Mul<TVector4<T>> for TMatrix4<T> {
    type Output = TVector4<T>;

    fn mul(self, v: TVector4<T>) -> Self::Output {
        self.x * v.x + self.y * v.y + self.z * v.z + self.w * v.w
    }
}

impl<T: Field<T>> Mul for TMatrix3<T> {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let x = self * rhs.x;
        let y = self * rhs.y;
        let z = self * rhs.z;

        Self::Output { x, y, z }
    }
}

impl<T: Field<T>> Mul for TMatrix4<T> {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        let x = self * rhs.x;
        let y = self * rhs.y;
        let z = self * rhs.z;
        let w = self * rhs.w;

        Self::Output { x, y, z, w }
    }
}*/

#[cfg(test)]
mod tests {

    use std::f32::consts::FRAC_PI_2;

    use tests::topology::MetricSpace;

    use crate::math::*;

    use super::super::vector::*;
    use super::*;

    #[test]
    fn multiplication() {
        let x = [5.0, 2.0, 8.0, 3.0];
        let y = [7.0, 3.0, 10.0, 3.0];
        let z = [9.0, 3.0, 2.0, 4.0];
        let w = [10.0, 8.0, 3.0, 8.0];
        let a = Matrix([x, y, z, w]);

        let x = [3.0, 12.0, 9.0, 3.0];
        let y = [10.0, 1.0, 10.0, 12.0];
        let z = [12.0, 4.0, 12.0, 4.0];
        let w = [18.0, 9.0, 2.0, 10.0];
        let b = Matrix([x, y, z, w]);

        let x = [210.0, 93.0, 171.0, 105.0];
        let y = [267.0, 149.0, 146.0, 169.0];
        let z = [236.0, 104.0, 172.0, 128.0];
        let w = [271.0, 149.0, 268.0, 169.0];
        let c = Matrix([x, y, z, w]);

        assert_eq!(a * b, c);
    }

    #[test]
    fn composition() {
        let a = Matrix::from_translation(Vector([1.0, 0.0, 0.0]));
        let b = Matrix::from_translation(Vector([0.0, 1.0, 0.0]));
        let c = Matrix::from_translation(Vector([0.0, 0.0, 1.0]));

        let d = Matrix::from_translation(Vector([1.0, 1.0, 1.0]));

        assert_eq!(a * b * c, d);
    }

    #[test]
    fn rotation_compare_with_quatertion() {
        let q = Quaterion::from_rotation(FRAC_PI_2, Vector3::OY);
        let rot = Matrix3::from_rotation(q);

        let v = Vector([1.0, 2.0, 3.0]);

        let lhs = rot * v;
        let rhs = q.rotate(v);

        assert!(Euclidean::distance(&lhs.0, &rhs.0) <= 1e-5);
    }

    #[test]
    fn projection() {
        let proj = Matrix4::from_perspective(FRAC_PI_2, 1.0, 1.0, 100.0);

        let v: Vector4 = Vector([0.0, 0.0, -0.1, 1.0]);
        assert!(Vector3::from(proj * v).z < 0.0);

        let v: Vector4 = Vector([0.0, 0.0, -20.5, 1.0]);
        assert!(Vector3::from(proj * v).z <= 1.0);
        assert!(Vector3::from(proj * v).z >= 0.0);

        let v: Vector4 = Vector([0.0, 0.0, -100.1, 1.0]);
        assert!(Vector3::from(proj * v).z > 1.0);
    }
}
