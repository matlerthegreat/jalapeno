use std::{
    mem::transmute,
    ops::{Deref, DerefMut, Div, Mul, MulAssign},
};

use super::{topology::Euclidean, vector::Vector, Scalar};

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Quaterion {
    pub r: Scalar,
    pub i: Scalar,
    pub j: Scalar,
    pub k: Scalar,
}

impl Deref for Quaterion {
    type Target = [Scalar; 4];

    fn deref(&self) -> &Self::Target {
        // SAFE because Quaternion is repr(C) and thus has the memory layout as an array of 4 Scalars
        unsafe { transmute(self) }
    }
}

impl DerefMut for Quaterion {
    fn deref_mut(&mut self) -> &mut Self::Target {
        // SAFE because Quaternion is repr(C) and thus has the memory layout as an array of 4 Scalars
        unsafe { transmute(self) }
    }
}

impl From<[Scalar; 4]> for Quaterion {
    fn from(value: [Scalar; 4]) -> Self {
        // SAFE because Quaternion is repr(C) and thus has the memory layout as an array of 4 Scalars
        unsafe { transmute(value) }
    }
}

impl Quaterion {
    pub const fn new(r: Scalar, i: Scalar, j: Scalar, k: Scalar) -> Self {
        Self { r, i, j, k }
    }

    pub const fn identity() -> Self {
        Self {
            r: 1.0,
            i: 0.0,
            j: 0.0,
            k: 0.0,
        }
    }

    pub fn into_parts(self) -> (Scalar, Vector<Euclidean, 3>) {
        (self.r, Vector([self.i, self.j, self.k]))
    }

    pub fn from_parts(r: Scalar, v: Vector<Euclidean, 3>) -> Self {
        Self {
            r,
            i: v.x,
            j: v.y,
            k: v.z,
        }
    }

    pub fn from_rotation(angle: Scalar, axis: Vector<Euclidean, 3>) -> Self {
        let (s, r) = (angle / 2.0).sin_cos();
        let v = s * axis.normalized();

        Self::from_parts(r, v)
    }

    pub fn from_rotations(angles: Vector<Euclidean, 3>) -> Self {
        let z = Self::from_rotation(angles.z, Vector::<Euclidean, 3>::OZ);
        let x = Self::from_rotation(angles.x, Vector::<Euclidean, 3>::OX);
        let y = Self::from_rotation(angles.y, Vector::<Euclidean, 3>::OY);

        z * x * y
    }

    pub fn conjugate(self) -> Self {
        let (s, v) = self.into_parts();
        Self::from_parts(s, -v)
    }

    pub fn norm_squared(self) -> Scalar {
        self.into_iter().map(|a| a.powi(2)).sum()
    }

    pub fn norm(self) -> Scalar {
        self.norm_squared().sqrt()
    }

    pub fn reciprocal(self) -> Self {
        self.conjugate() / self.norm_squared()
    }

    pub fn rotate(self, v: Vector<Euclidean, 3>) -> Vector<Euclidean, 3> {
        let p = Self::from_parts(0.0, v);
        (self * p * self.conjugate()).into_parts().1
    }
}

impl Default for Quaterion {
    fn default() -> Self {
        Self::identity()
    }
}

impl Mul for Quaterion {
    type Output = Quaterion;

    fn mul(self, rhs: Self) -> Self::Output {
        let (r1, v1) = self.into_parts();
        let (r2, v2) = rhs.into_parts();
        Self::from_parts(r1 * r2 - v1.dot(v2), r1 * v2 + r2 * v1 + v1.cross(v2))
    }
}

impl MulAssign for Quaterion {
    fn mul_assign(&mut self, rhs: Self) {
        *self = *self * rhs;
    }
}

impl Div<Scalar> for Quaterion {
    type Output = Quaterion;

    fn div(self, rhs: Scalar) -> Self::Output {
        self.map(|r| r / rhs).into()
    }
}

#[cfg(test)]
mod tests {
    use std::f32::consts::PI;

    use crate::math::topology::*;
    use crate::math::*;

    use super::*;

    #[test]
    fn mul() {
        let p = Quaterion::new(1.0, 2.0, 3.0, 4.0);
        let q = Quaterion::new(5.0, 6.0, 7.0, 8.0);

        let r = p.r * q.r - p.i * q.i - p.j * q.j - p.k * q.k;
        let i = p.r * q.i + p.i * q.r + p.j * q.k - p.k * q.j;
        let j = p.r * q.j - p.i * q.k + p.j * q.r + p.k * q.i;
        let k = p.r * q.k + p.i * q.j - p.j * q.i + p.k * q.r;

        let expected = Quaterion::new(r, i, j, k);

        assert_eq!(p * q, expected);
    }

    #[test]
    fn rotate() {
        let axis = Vector3::OZ;
        let angle = PI / 2.0;
        let r = Quaterion::from_rotation(angle, axis);
        let v = Vector3::OX;

        let expected = Vector3::OY;

        assert!(Euclidean::distance(r.rotate(v), expected) < 1e-3);
    }

    #[test]
    fn compond_rotate() {
        let angle = PI / 2.0;
        let p = Quaterion::from_rotation(angle, Vector3::OZ);
        let q = Quaterion::from_rotation(angle, Vector3::OX);

        let r = q * p;

        let v = Vector3::OX;

        let expected = Vector3::OZ;

        assert!(Euclidean::distance(r.rotate(v), expected) < 1e-3);
    }

    #[test]
    fn rotate_identity() {
        let r = Quaterion::identity();

        assert!(Euclidean::distance(r.rotate(Vector3::OX), Vector3::OX) < 1e-3);
        assert!(Euclidean::distance(r.rotate(Vector3::OY), Vector3::OY) < 1e-3);
        assert!(Euclidean::distance(r.rotate(Vector3::OZ), Vector3::OZ) < 1e-3);
    }

    #[test]
    fn compound_rotate_identity() {
        let angle = PI / 2.0;
        let p = Quaterion::from_rotation(angle, Vector3::OZ);
        let q = Quaterion::from_rotation(angle, Vector3::OX);

        let r = Quaterion::identity() * q * Quaterion::identity() * p * Quaterion::identity();

        let v = Vector3::OX;

        let expected = Vector3::OZ;

        assert!(Euclidean::distance(r.rotate(v), expected) < 1e-3);
    }
}
