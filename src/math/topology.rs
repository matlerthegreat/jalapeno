use std::borrow::{Borrow, BorrowMut};

use super::{Field, Integer, Scalar};

pub trait Space<const D: usize> {
    type Coordinate;
    type Coordinates;

    fn as_coordinates(src: &[Self::Coordinate; D]) -> &Self::Coordinates;
    fn as_mut_coordinates(src: &mut [Self::Coordinate; D]) -> &mut Self::Coordinates;
}

pub trait VectorSpace<const D: usize>: Sized + Space<D> {
    type F: Field<Self::F>;

    fn add(
        a: impl Borrow<[Self::Coordinate; D]>,
        b: impl Borrow<[Self::Coordinate; D]>,
    ) -> [Self::Coordinate; D];
    fn add_assign(a: impl BorrowMut<[Self::Coordinate; D]>, b: impl Borrow<[Self::Coordinate; D]>);
    fn sub(
        a: impl Borrow<[Self::Coordinate; D]>,
        b: impl Borrow<[Self::Coordinate; D]>,
    ) -> [Self::Coordinate; D];
    fn sub_assign(a: impl BorrowMut<[Self::Coordinate; D]>, b: impl Borrow<[Self::Coordinate; D]>);

    fn mul(v: impl Borrow<[Self::Coordinate; D]>, s: Self::F) -> [Self::Coordinate; D];
    fn mul_assign(v: impl BorrowMut<[Self::Coordinate; D]>, s: Self::F);
    fn div(v: impl Borrow<[Self::Coordinate; D]>, s: Self::F) -> [Self::Coordinate; D];
    fn div_assign(v: impl BorrowMut<[Self::Coordinate; D]>, s: Self::F);
}

pub trait MetricSpace<const D: usize>: VectorSpace<D> {
    fn length_squared(v: impl Borrow<[Self::Coordinate; D]> + Copy) -> Self::F;
    fn length(v: impl Borrow<[Self::Coordinate; D]> + Copy) -> Self::F;

    fn distance_squared(
        a: impl Borrow<[Self::Coordinate; D]>,
        b: impl Borrow<[Self::Coordinate; D]>,
    ) -> Self::F {
        Self::length_squared(&Self::sub(a, b))
    }
    fn distance(
        a: impl Borrow<[Self::Coordinate; D]>,
        b: impl Borrow<[Self::Coordinate; D]>,
    ) -> Self::F {
        Self::length(&Self::sub(a, b))
    }
}

pub trait InnerProductSpace<const D: usize>: Sized + VectorSpace<D>
where
    Self::Coordinate: Field<Self::Coordinate>,
{
    fn dot(
        v: impl Borrow<[Self::Coordinate; D]>,
        w: impl Borrow<[Self::Coordinate; D]>,
    ) -> Self::Coordinate {
        v.borrow()
            .iter()
            .zip(w.borrow())
            .map(|(a, b)| *a * *b)
            .reduce(|a, b| a + b)
            .unwrap()
    }
}

#[macro_export]
macro_rules! componentwise_vector_space_impl {
    (over $f:ty, for $t:ty, $d:expr) => {
        impl VectorSpace<$d> for $t {
            type F = $f;

            fn add(
                a: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
                b: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
            ) -> [Self::Coordinate; $d] {
                core::array::from_fn(|i| a.borrow()[i] + b.borrow()[i])
            }

            fn add_assign(
                mut a: impl core::borrow::BorrowMut<[Self::Coordinate; $d]>,
                b: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
            ) {
                for i in 0..$d {
                    a.borrow_mut()[i] += b.borrow()[i]
                }
            }

            fn sub(
                a: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
                b: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
            ) -> [Self::Coordinate; $d] {
                core::array::from_fn(|i| a.borrow()[i] - b.borrow()[i])
            }

            fn sub_assign(
                mut a: impl core::borrow::BorrowMut<[Self::Coordinate; $d]>,
                b: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
            ) {
                for i in 0..$d {
                    a.borrow_mut()[i] -= b.borrow()[i]
                }
            }

            fn mul(
                v: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
                s: Self::F,
            ) -> [Self::Coordinate; $d] {
                core::array::from_fn(|i| v.borrow()[i] * s)
            }

            fn mul_assign(mut v: impl core::borrow::BorrowMut<[Self::Coordinate; $d]>, s: Self::F) {
                for i in 0..$d {
                    v.borrow_mut()[i] *= s
                }
            }

            fn div(
                v: impl core::borrow::Borrow<[Self::Coordinate; $d]>,
                s: Self::F,
            ) -> [Self::Coordinate; $d] {
                core::array::from_fn(|i| v.borrow()[i] / s)
            }

            fn div_assign(mut v: impl core::borrow::BorrowMut<[Self::Coordinate; $d]>, s: Self::F) {
                for i in 0..$d {
                    v.borrow_mut()[i] /= s
                }
            }
        }
    };
}

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Euclidean;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Projective;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Digital;

#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Color;

#[derive(Debug, PartialEq, Eq, Hash)]
struct Texture;

#[repr(C)]
pub struct XyVector<T> {
    pub x: T,
    pub y: T,
}

#[repr(C)]
pub struct XyzVector<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

#[repr(C)]
pub struct XyzwVector<T> {
    pub x: T,
    pub y: T,
    pub z: T,
    pub w: T,
}

#[repr(C)]
struct UvCoordinates<T> {
    pub u: T,
    pub v: T,
}

#[repr(C)]
pub struct RgbVector<T> {
    pub r: T,
    pub g: T,
    pub b: T,
}

#[repr(C)]
pub struct RgbaVector<T> {
    pub r: T,
    pub g: T,
    pub b: T,
    pub a: T,
}

impl Space<1> for Euclidean {
    type Coordinate = Scalar;
    type Coordinates = Scalar;

    fn as_coordinates(src: &[Self::Coordinate; 1]) -> &Self::Coordinates {
        &src[0]
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 1]) -> &mut Self::Coordinates {
        &mut src[0]
    }
}

impl Space<2> for Euclidean {
    type Coordinate = Scalar;
    type Coordinates = XyVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 2]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 2]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl Space<3> for Euclidean {
    type Coordinate = Scalar;
    type Coordinates = XyzVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 3]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 3]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl Space<4> for Euclidean {
    type Coordinate = Scalar;
    type Coordinates = XyzwVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 4]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 4]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl MetricSpace<3> for Euclidean {
    fn length_squared(v: impl Borrow<[Scalar; 3]> + Copy) -> Self::F {
        Self::dot(v, v)
    }

    fn length(v: impl Borrow<[Scalar; 3]> + Copy) -> Self::F {
        Self::length_squared(v).sqrt()
    }
}

impl MetricSpace<3> for Digital {
    fn length(v: impl Borrow<[Integer; 3]> + Copy) -> Self::F {
        v.borrow().iter().map(|i| i.abs()).sum()
    }

    fn length_squared(v: impl Borrow<[Integer; 3]> + Copy) -> Self::Coordinate {
        Self::length(v).pow(2)
    }
}

componentwise_vector_space_impl!(over Scalar, for Euclidean, 1);
componentwise_vector_space_impl!(over Scalar, for Euclidean, 2);
componentwise_vector_space_impl!(over Scalar, for Euclidean, 3);
componentwise_vector_space_impl!(over Scalar, for Euclidean, 4);

impl InnerProductSpace<1> for Euclidean {}
impl InnerProductSpace<2> for Euclidean {}
impl InnerProductSpace<3> for Euclidean {}
impl InnerProductSpace<4> for Euclidean {}

impl Space<1> for Projective {
    type Coordinate = Scalar;
    type Coordinates = Scalar;

    fn as_coordinates(src: &[Self::Coordinate; 1]) -> &Self::Coordinates {
        &src[0]
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 1]) -> &mut Self::Coordinates {
        &mut src[0]
    }
}

impl Space<2> for Projective {
    type Coordinate = Scalar;
    type Coordinates = XyVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 2]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 2]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl Space<3> for Projective {
    type Coordinate = Scalar;
    type Coordinates = XyzVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 3]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 3]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl Space<4> for Projective {
    type Coordinate = Scalar;
    type Coordinates = XyzwVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 4]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 4]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

componentwise_vector_space_impl!(over Scalar, for Projective, 1);
componentwise_vector_space_impl!(over Scalar, for Projective, 2);
componentwise_vector_space_impl!(over Scalar, for Projective, 3);
componentwise_vector_space_impl!(over Scalar, for Projective, 4);

impl Space<1> for Digital {
    type Coordinate = Integer;
    type Coordinates = Integer;

    fn as_coordinates(src: &[Self::Coordinate; 1]) -> &Self::Coordinates {
        &src[0]
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 1]) -> &mut Self::Coordinates {
        &mut src[0]
    }
}

impl Space<2> for Digital {
    type Coordinate = Integer;
    type Coordinates = XyVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 2]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 2]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl Space<3> for Digital {
    type Coordinate = Integer;
    type Coordinates = XyzVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 3]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 3]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl Space<4> for Digital {
    type Coordinate = Integer;
    type Coordinates = XyzwVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 4]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 4]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

componentwise_vector_space_impl!(over Integer, for Digital, 1);
componentwise_vector_space_impl!(over Integer, for Digital, 2);
componentwise_vector_space_impl!(over Integer, for Digital, 3);
componentwise_vector_space_impl!(over Integer, for Digital, 4);

impl Space<2> for Texture {
    type Coordinate = usize;
    type Coordinates = UvCoordinates<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 2]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 2]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

componentwise_vector_space_impl!(over usize, for Texture, 2);

impl Space<3> for Color {
    type Coordinate = u8;
    type Coordinates = RgbVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 3]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 3]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

impl Space<4> for Color {
    type Coordinate = u8;
    type Coordinates = RgbaVector<Self::Coordinate>;

    fn as_coordinates(src: &[Self::Coordinate; 4]) -> &Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }

    fn as_mut_coordinates(src: &mut [Self::Coordinate; 4]) -> &mut Self::Coordinates {
        unsafe { std::mem::transmute(src) }
    }
}

componentwise_vector_space_impl!(over u8, for Color, 3);
componentwise_vector_space_impl!(over u8, for Color, 4);
