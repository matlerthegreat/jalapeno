use std::{
    array,
    borrow::{Borrow, BorrowMut},
    fmt::Debug,
    ops::{
        Add, AddAssign, Deref, DerefMut, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Sub,
        SubAssign,
    },
};

use super::{
    topology::{MetricSpace, Space, VectorSpace},
    vector::Vector,
};

#[repr(transparent)]
#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Point<S: ?Sized + Space<D>, const D: usize>(pub [S::Coordinate; D]);

impl<const D: usize, S: MetricSpace<D>> Point<S, D> {
    pub fn distance(self, other: Self) -> S::F {
        S::distance(self, other)
    }
    pub fn distance_squared(self, other: Self) -> S::F {
        S::distance_squared(self, other)
    }
}

impl<const D: usize, S: Space<D>> Default for Point<S, D>
where
    S::Coordinate: Default,
{
    fn default() -> Self {
        Self(array::from_fn(|_| Default::default()))
    }
}

impl<const D: usize, S: Space<D>> Clone for Point<S, D>
where
    S::Coordinate: Clone,
{
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

impl<const D: usize, S: Space<D>> Copy for Point<S, D> where S::Coordinate: Copy {}

impl<const D: usize, S: Space<D>> Deref for Point<S, D> {
    type Target = S::Coordinates;

    fn deref(&self) -> &Self::Target {
        S::as_coordinates(&self.0)
    }
}

impl<const D: usize, S: Space<D>> DerefMut for Point<S, D> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        S::as_mut_coordinates(&mut self.0)
    }
}

impl<const D: usize, S: Space<D>> Borrow<[S::Coordinate; D]> for Point<S, D> {
    fn borrow(&self) -> &[S::Coordinate; D] {
        &self.0
    }
}

impl<const D: usize, S: Space<D>> BorrowMut<[S::Coordinate; D]> for Point<S, D> {
    fn borrow_mut(&mut self) -> &mut [S::Coordinate; D] {
        &mut self.0
    }
}

impl<const D: usize, S: Space<D>> Borrow<[S::Coordinate; D]> for &Point<S, D> {
    fn borrow(&self) -> &[S::Coordinate; D] {
        &self.0
    }
}

impl<const D: usize, S: Space<D>> Borrow<[S::Coordinate; D]> for &mut Point<S, D> {
    fn borrow(&self) -> &[S::Coordinate; D] {
        &self.0
    }
}

impl<const D: usize, S: Space<D>> BorrowMut<[S::Coordinate; D]> for &mut Point<S, D> {
    fn borrow_mut(&mut self) -> &mut [S::Coordinate; D] {
        &mut self.0
    }
}

impl<const D: usize, S: Space<D>> From<[S::Coordinate; D]> for Point<S, D> {
    fn from(value: [S::Coordinate; D]) -> Self {
        Self(value)
    }
}

impl<const D: usize, S: Space<D>> Index<usize> for Point<S, D> {
    type Output = S::Coordinate;

    fn index(&self, index: usize) -> &Self::Output {
        &self.0[index]
    }
}

impl<const D: usize, S: Space<D>> IndexMut<usize> for Point<S, D> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.0[index]
    }
}

impl<const D: usize, S: Space<D>> IntoIterator for Point<S, D> {
    type Item = S::Coordinate;

    type IntoIter = array::IntoIter<Self::Item, D>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<const D: usize, S: Space<D>> From<Vector<S, D>> for Point<S, D> {
    fn from(value: Vector<S, D>) -> Self {
        Point(value.0)
    }
}

impl<const D: usize, S: VectorSpace<D>> Sub for Point<S, D> {
    type Output = Vector<S, D>;

    fn sub(self, rhs: Self) -> Self::Output {
        Vector(S::sub(self, rhs))
    }
}

impl<const D: usize, S: VectorSpace<D>> Add<Vector<S, D>> for Point<S, D> {
    type Output = Self;

    fn add(self, rhs: Vector<S, D>) -> Self::Output {
        Point(S::add(self, rhs))
    }
}

impl<const D: usize, S: VectorSpace<D>> AddAssign<Vector<S, D>> for Point<S, D> {
    fn add_assign(&mut self, rhs: Vector<S, D>) {
        S::add_assign(self, rhs)
    }
}

impl<const D: usize, S: VectorSpace<D>> Sub<Vector<S, D>> for Point<S, D> {
    type Output = Self;

    fn sub(self, rhs: Vector<S, D>) -> Self::Output {
        Point(S::sub(self, rhs))
    }
}

impl<const D: usize, S: VectorSpace<D>> SubAssign<Vector<S, D>> for Point<S, D> {
    fn sub_assign(&mut self, rhs: Vector<S, D>) {
        S::sub_assign(self, rhs)
    }
}

impl<const D: usize, S: VectorSpace<D>> Mul<Point<S, D>> for S::F {
    type Output = Point<S, D>;

    fn mul(self, rhs: Point<S, D>) -> Self::Output {
        Point(S::mul(rhs, self))
    }
}

impl<const D: usize, S: VectorSpace<D>> MulAssign<S::F> for Point<S, D> {
    fn mul_assign(&mut self, rhs: S::F) {
        S::mul_assign(self, rhs)
    }
}

impl<const D: usize, S: VectorSpace<D>> Div<S::F> for Point<S, D> {
    type Output = Self;

    fn div(self, rhs: S::F) -> Self::Output {
        Point(S::div(self, rhs))
    }
}

impl<const D: usize, S: VectorSpace<D>> DivAssign<S::F> for Point<S, D> {
    fn div_assign(&mut self, rhs: S::F) {
        S::div_assign(self, rhs)
    }
}
