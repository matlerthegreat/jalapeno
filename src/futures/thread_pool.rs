use std::{
    future::Future,
    pin::{pin, Pin},
    sync::{Arc, Mutex},
    task::{Context, Poll, Wake},
    thread,
};

use super::{block_on::block_on, oneshot, unbounded};

pub struct Task {
    future: Mutex<Option<Pin<Box<dyn Future<Output = ()> + Send>>>>,
    task_sender: unbounded::Sender<Arc<Task>>,
}

impl Wake for Task {
    fn wake(self: Arc<Self>) {
        self.task_sender.send(self.clone());
    }

    fn wake_by_ref(self: &Arc<Self>) {
        self.task_sender.send(self.clone());
    }
}

#[derive(Clone)]
pub struct Spawner {
    tx: unbounded::Sender<Arc<Task>>,
}

impl Spawner {
    pub fn spawn<T: Send + 'static>(
        &self,
        future: impl Future<Output = T> + Send + 'static,
    ) -> TaskHandle<T> {
        let (tx, rx) = oneshot::channel();
        let future = Box::pin(async move {
            tx.send(future.await);
        });
        let runnable = Arc::new(Task {
            future: Mutex::new(Some(future)),
            task_sender: self.tx.clone(),
        });
        self.tx.send(runnable);

        TaskHandle { rx }
    }
}

pub struct Worker {
    rx: unbounded::Receiver<Arc<Task>>,
}

pub struct TaskHandle<T> {
    rx: oneshot::Receiver<T>,
}

impl<T> Future for TaskHandle<T> {
    type Output = T;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let rec = pin!(self.rx.recv());
        match rec.poll(cx) {
            Poll::Ready(Some(value)) => Poll::Ready(value),
            Poll::Pending => Poll::Pending,
            _ => panic!(),
        }
    }
}

impl Worker {
    fn run(self) {
        while let Some(task) = self.rx.recv().block() {
            let mut future_slot = task.future.lock().unwrap();
            if let Some(mut future) = future_slot.take() {
                let waker = task.clone().into();
                let mut context = Context::from_waker(&waker);
                if future.as_mut().poll(&mut context).is_pending() {
                    *future_slot = Some(future);
                }
            }
        }
    }
}

/// Creates a thread pool with `n` threads that are used to spawn async tasks. The calling thread blocks on the provided `future`.
pub fn thread_pool<T, F: Future<Output = T>>(n: usize, future: impl FnOnce(Spawner) -> F) -> T {
    let (tx, rx) = unbounded::channel();
    let spawner = Spawner { tx };
    thread::scope(|scope| {
        for _ in 0..n {
            let worker = Worker { rx: rx.clone() };
            thread::Builder::new()
                .name(format!("Worker#{n}"))
                .spawn_scoped(scope, || worker.run())
                .unwrap();
        }
        block_on(future(spawner))
    })
}

#[cfg(test)]
mod tests {
    use std::{thread, time::Duration};

    use super::*;

    #[test]
    fn multi_threaded() {
        thread_pool(2, |spawner| async move {
            let one = spawner.spawn(async {
                println!("one!");
                thread::sleep(Duration::from_secs(1));
                1
            });
            let two = spawner.spawn(async {
                println!("two!");
                thread::sleep(Duration::from_secs(1));
                2
            });
            let three = spawner.spawn(async {
                println!("three!");
                thread::sleep(Duration::from_secs(1));
                3
            });

            assert_eq!(one.await, 1);
            assert_eq!(two.await, 2);
            assert_eq!(three.await, 3);
        });
    }
}
