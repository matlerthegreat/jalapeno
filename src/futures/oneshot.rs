use std::{
    future::Future,
    pin::Pin,
    sync::{Arc, Mutex},
    task::{Context, Poll, Waker},
};

use super::block_on::block_on;

pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let channel = Arc::new(Mutex::new(Channel {
        value: Option::None,
        waker: Option::None,
        senders_count: true,
    }));

    let sender = Sender {
        channel: channel.clone(),
    };
    let recvr = Receiver { channel };

    (sender, recvr)
}

struct Channel<T> {
    value: Option<T>,
    waker: Option<Waker>,
    senders_count: bool,
}

pub struct Sender<T> {
    channel: Arc<Mutex<Channel<T>>>,
}

impl<T> Sender<T> {
    pub fn send(self, value: T) {
        let mut channel = self.channel.lock().unwrap();
        let _ = channel.value.insert(value);
        if let Some(waker) = channel.waker.take() {
            waker.wake()
        }
    }
}

impl<T> Drop for Sender<T> {
    fn drop(&mut self) {
        let mut channel = self.channel.lock().unwrap();
        channel.senders_count = false;
    }
}

pub struct Receiver<T> {
    channel: Arc<Mutex<Channel<T>>>,
}

impl<T> Receiver<T> {
    pub fn recv(&self) -> Recieve<T> {
        Recieve { owner: self }
    }
}

pub struct Recieve<'a, T> {
    owner: &'a Receiver<T>,
}

impl<'a, T> Recieve<'a, T> {
    pub fn block(self) -> Option<T> {
        block_on(self)
    }
}

impl<'a, T> Future for Recieve<'a, T> {
    type Output = Option<T>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut channel = self.owner.channel.lock().unwrap();
        if let Some(value) = channel.value.take() {
            Poll::Ready(Some(value))
        } else if !channel.senders_count {
            Poll::Ready(None)
        } else {
            let _ = channel.waker.insert(cx.waker().clone());
            Poll::Pending
        }
    }
}

impl<T> Clone for Receiver<T> {
    fn clone(&self) -> Self {
        Self {
            channel: self.channel.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{thread, time::Duration};

    use crate::futures::block_on::block_on;

    use super::*;

    #[test]
    fn channels() {
        let (tx, rx) = channel();

        let t = thread::spawn(move || {
            block_on(async {
                while let Some(val) = rx.recv().await {
                    println!("{val}");
                }
            })
        });

        thread::sleep(Duration::from_secs(5));

        tx.send(1);

        t.join().unwrap()
    }
}
