use std::{future::Future, pin::{Pin, pin}, task::{Context, Poll}};

pub struct Yield(bool);

pub fn yield_now() -> Yield {
    Yield(false)
}

impl Future for Yield {
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut yielded = pin!(self);
        if yielded.0 {
            Poll::Ready(())
        } else {
            yielded.0 = true;
            cx.waker().wake_by_ref();
            Poll::Pending
        }
    }
}