use std::{
    collections::VecDeque,
    future::Future,
    pin::Pin,
    sync::{Arc, Mutex},
    task::{Context, Poll, Waker}, num::NonZeroUsize, ops::DerefMut,
};

use super::block_on::block_on;

pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let channel = Arc::new(Mutex::new(Channel::Open {
        values: VecDeque::new(),
        wakers: VecDeque::new(),
        senders_count: unsafe {NonZeroUsize::new_unchecked(1)},
    }));

    let sender = Sender {
        channel: channel.clone(),
    };
    let recvr = Receiver { channel };

    (sender, recvr)
}

enum Channel<T> {
    Open{
        senders_count: NonZeroUsize,
        values: VecDeque<T>,
        wakers: VecDeque<Waker>,
    },
    Closed
}

pub struct Sender<T> {
    channel: Arc<Mutex<Channel<T>>>,
}

impl<T> Sender<T> {
    pub fn send(&self, value: T) {
        let mut channel = self.channel.lock().unwrap();
        if let Channel::Open{values, wakers, ..} = channel.deref_mut() {
            values.push_back(value);
            if let Some(waker) = wakers.pop_front() {
                waker.wake()
            }
        }
    }
}

impl<T> Clone for Sender<T> {
    fn clone(&self) -> Self {
        let mut channel = self.channel.lock().unwrap();
        if let Channel::Open{senders_count, ..} = channel.deref_mut() {
            senders_count.checked_add(1).unwrap();
        }
        
        Self {
            channel: self.channel.clone(),
        }
    }
}

impl<T> Drop for Sender<T> {
    fn drop(&mut self) {
        let mut channel = self.channel.lock().unwrap();
        if let Channel::Open{senders_count, ..} = channel.deref_mut() {
            let new_count = senders_count.get() - 1;
            if new_count > 0 {
                *senders_count = unsafe { NonZeroUsize::new_unchecked(new_count) };
            } else {
                let old_channel = std::mem::replace(channel.deref_mut(), Channel::Closed);
                if let Channel::Open{wakers, ..} = old_channel {
                    wakers.into_iter().for_each(|waker| waker.wake());
                }
            }
        }
    }
}

pub struct Receiver<T> {
    channel: Arc<Mutex<Channel<T>>>,
}

impl<T> Receiver<T> {
    pub fn recv(&self) -> Recieve<T> {
        Recieve { owner: self }
    }
}

pub struct Recieve<'a, T> {
    owner: &'a Receiver<T>,
}

impl<'a, T> Recieve<'a, T> {
    pub fn block(self) -> Option<T> {
        block_on(self)
    }
}

impl<'a, T> Future for Recieve<'a, T> {
    type Output = Option<T>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut channel = self.owner.channel.lock().unwrap();
        if let Channel::Open{values, wakers, ..} = channel.deref_mut() {
            if let Some(value) = values.pop_front() {
                Poll::Ready(Some(value))
            } else {
                wakers.push_back(cx.waker().clone());
                Poll::Pending
            }
        } else {
            Poll::Ready(None)
        }
    }
}

impl<T> Clone for Receiver<T> {
    fn clone(&self) -> Self {
        Self {
            channel: self.channel.clone(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{thread, time::Duration};

    use crate::futures::block_on::block_on;

    use super::*;

    #[test]
    fn channels() {
        let (tx, rx) = channel();

        let t = thread::spawn(move || {
            block_on(async {
                while let Some(val) = rx.recv().await {
                    println!("{val}");
                }
            })
        });

        thread::sleep(Duration::from_secs(5));

        tx.send(1);
        tx.send(2);
        tx.send(3);
        drop(tx);

        t.join().unwrap()
    }

    #[test]
    fn map_reduce() {
        let (tx, rx) = channel();

        let t1 = {
            let rx = rx.clone();
            thread::spawn(move || {
                block_on(async move {
                    let mut sum = 0;
                    while let Some(n) = rx.recv().await {
                        sum += n * n;
                        thread::sleep(Duration::from_micros(1))
                    }
                    sum
                })
            })
        };
        let t2 = {
            let rx = rx.clone();
            thread::spawn(move || {
                block_on(async move {
                    let mut sum = 0;
                    while let Some(n) = rx.recv().await {
                        sum += n * n;
                        thread::sleep(Duration::from_micros(1))
                    }
                    sum
                })
            })
        };
        let t3 = {
            let rx = rx.clone();
            thread::spawn(move || {
                block_on(async move {
                    let mut sum = 0;
                    while let Some(n) = rx.recv().await {
                        sum += n * n;
                        thread::sleep(Duration::from_micros(1))
                    }
                    sum
                })
            })
        };

        for n in 0..100 {
            tx.send(n);
        }
        drop(tx);

        let v1 = t1.join().unwrap();
        let v2 = t2.join().unwrap();
        let v3 = t3.join().unwrap();

        println!("{v1}");
        println!("{v2}");
        println!("{v3}");

        let v = v1 + v2 + v3;

        println!("{v}")
    }

    #[test]
    fn generate_map_reduce() {
        let (generator_tx, generator_rx) = channel();
        let (mapper_tx, mapper_rx) = channel();

        let generator = move || {
            for i in 0..1000 {
                generator_tx.send(i);
            }
        };
        let mapper = |tx: Sender<i32>| {
            let generator_rx = generator_rx.clone();
            block_on(async move {
                while let Some(value) = generator_rx.recv().await {
                    tx.send(value * value);
                }
                drop(tx);
            });
        };
        let reducer = async move {
            let mut sum = 0;
            while let Some(value) = mapper_rx.recv().await {
                sum += value;
            }
            sum
        };

        let sum = thread::scope(move |s| {
            s.spawn(generator);
            for tx in std::iter::repeat(mapper_tx).take(4) {
                s.spawn(move || mapper(tx));
            }
            block_on(reducer)
        });

        assert_eq!(sum, 332833500);
    }
}
