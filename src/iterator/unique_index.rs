struct BitSet<const N: usize> {
    flags: [usize; N],
}

impl<const N: usize> BitSet<N> {
    pub fn new() -> Self {
        BitSet { flags: [0; N] }
    }

    pub fn set(&mut self, bit: usize) -> bool {
        let index = bit / usize::BITS as usize;
        let offset = bit - (index * usize::BITS as usize);
        let ret = !self.test(bit);
        self.flags[index] |= 1 << offset;
        ret
    }

    pub fn test(&self, bit: usize) -> bool {
        let index = bit / usize::BITS as usize;
        let offset = bit - (index * usize::BITS as usize);
        self.flags[index] & (1 << offset) != 0
    }
}

impl<const N: usize> Default for BitSet<N> {
    fn default() -> Self {
        Self::new()
    }
}

pub struct UniqueIndex<I, const N: usize>
where
    I: Iterator,
{
    visited: BitSet<N>,
    inner: I,
}

impl<I, const N: usize> Iterator for UniqueIndex<I, N>
where
    I: Iterator<Item = usize>,
{
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.by_ref().find(|&value| self.visited.set(value))
    }
}

pub trait UniqueIndexExt: Iterator<Item = usize> {
    fn unique_index<const N: usize>(self) -> UniqueIndex<Self, N>
    where
        Self: Sized,
    {
        UniqueIndex {
            visited: BitSet::new(),
            inner: self,
        }
    }
}

impl<I: Iterator<Item = usize>> UniqueIndexExt for I {}
