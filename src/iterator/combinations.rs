pub struct Combinations<I>
where
    I: Iterator + Clone,
    I::Item: Copy,
{
    first: I,
    second: I,
    value: Option<I::Item>,
}

impl<I> Iterator for Combinations<I>
where
    I: Iterator + Clone,
    I::Item: Copy,
{
    type Item = (I::Item, I::Item);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(x) = self.value {
            if let Some(y) = self.second.next() {
                Some((x, y))
            } else {
                self.value = self.first.next();
                self.second = self.first.clone();
                self.next()
            }
        } else {
            None
        }
    }
}

pub trait CombinationsExt: Iterator + Clone
where
    Self::Item: Copy,
{
    fn combinations(mut self) -> Combinations<Self>
    where
        Self: Sized,
    {
        Combinations {
            value: self.next(),
            first: self.clone(),
            second: self,
        }
    }
}

// Iterate through all pairwise combinations in iterator
impl<I> CombinationsExt for I
where
    I: Iterator + Clone,
    I::Item: Copy,
{
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_test() {
        let array = [1, 2, 3, 4];

        let pairs: Vec<_> = array.into_iter().combinations().collect();

        assert_eq!(pairs, [(1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4)]);
    }
}
